from docx import *

files = [
	'Cheat Sheet 1.6.14',
	'EdElements_Research',
	'JD_CST_Manager_04092014_V2',
	'NACSA Cyber Issue Brief 10-04-11.alm',
	'PI for NACSA Policy Guide DRAFT 10 4 11_clean alm 10 4'
	]

for i, f in enumerate(files):
	files[i] = "data/{}.docx".format(f)

print(files)

def combine_word_documents(files):
	combined_document = Document('data/empty.docx')
	count, number_of_files = 0, len(files)

	for f in files:
		sub_doc = Document(f)

		print(sub_doc)

		if count < number_of_files - 1:
			sub_doc.add_page_break()

		print("{} documents processed so far...".format(count))

		body = sub_doc.xpath('/w:document/w:body', namespaces=nsprefixes)[0]
		combined_document.append(body)


		for i, section in enumerate(sub_doc.sections):
			print("Found another section! {}".format(i+1))

		count += 1
	combined_document.save("output/combined_word_documents.docx")

combine_word_documents(files)